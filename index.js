function docReady(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}
async function getTabImg() {
    let tabImg2 = await fetch('images.json')
    let tabImg = await tabImg2.json()
    return tabImg
}

function addImg(imgObj) {
    var img = document.createElement("img");
    img.classList.add("imgAdd")
    img.src = imgObj.src;
    img.alt = imgObj.title
    document.getElementById('container-img').appendChild(img);
}
window.addEventListener('online', event => {
    document.getElementById("offline").classList.add("hidden")
});
window.addEventListener('offline', event => {
    document.getElementById("offline").classList.remove("hidden")
});


docReady(async function () {
    if(navigator.onLine) {
        console.log("online")
        document.getElementById("offline").classList.add("hidden")
    } else {
        console.log("offline")
        document.getElementById("offline").classList.remove("hidden")
    }
    var tabImg = await getTabImg()
    var indexImag = 0
    document.getElementById('addImg').addEventListener("click", function () {
        if (indexImag < tabImg.length) {
            addImg(tabImg[indexImag])
            indexImag++
        } else {
            this.setAttribute('disabled', true)
            var warn = document.createElement('h2')
            warn.classList.add("warn")
            warn.textContent = "Toutes les images ont été affichées"
            document.getElementById('container-add').appendChild(warn)
        }
    });

});

